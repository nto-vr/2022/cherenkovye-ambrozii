extends Spatial

var speed = 2

func _physics_process(delta):
	var to = Vector3($KinematicBody/ARVROrigin/ARVRController.get_joystick_axis(0)*speed,0,$KinematicBody/ARVROrigin/ARVRController.get_joystick_axis(1)*-speed)
	$KinematicBody.move_and_slide(to)
	#print($KinematicBody/ARVROrigin/ARVRController.get_joystick_axis(0))

	$AudioStreamPlayer3D.unit_db = load("res://Ui/menu.tscn").instance().volume


func _on_Area_body_entered(body):
	$KinematicBody/ARVROrigin.can_shot = true

func _on_Area_area_shape_exited(area_rid, area, area_shape_index, local_shape_index):
	$KinematicBody/ARVROrigin.can_shot = false

func shot():
	var bolt = load("res://Game/Bolt.tscn")
	bolt = bolt.instance()
	add_child(bolt)
	bolt.translation = $final_barrel.translation
	bolt.move_and_slide(($final_barrel/Position3D.translation-bolt.translation)*10)
