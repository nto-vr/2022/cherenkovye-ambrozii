extends Spatial

var volume = 50


func _on_HSlider_value_changed(value):
	volume = value


func _on_Button2_pressed():
	get_tree().quit()


func _on_Button_pressed():
	get_tree().reload_current_scene()

func _on_Button3_pressed():
	get_tree().change_scene_to(load("res://Game/GameScene.tscn"))
